<?php 
if(isset($_POST['rest']))
{
$eif_settings = array();
$eif_settings = array(
    'eif_access_token' => '',
    'eif_feed_width' => '100',
	'eif_hastag'=> '',
	'eif_serach_type' => 'user',
    'eif_feed_width_unit' => '%',
    'eif_feed_height' => '100',
    'eif_feed_height_unit' => '%',
    'eif_feed_background_color' => '#fff',
	'eif_feed_image_padding_unit'=> 'px',
	'eif_feed_image_padding_size' => '5',
	'eif_feed_column_numbers'=> '4',
	'eif_feed_number_of_images' => '20',
	'eif_feed_image_resolution' => 'low_resolution',
	'eif_feed_image_sorting' => 'newer',
	'eif_feed_show_button_status'=>'no',
	'eif_feed_button_background_color' => '#000000',
	'eif_feed_button_text_color'=> '#fff',
	'eif_feed_follow_button_text' => 'Follow on Instagram',
	'eif_feed_load_more_button_status'=> 'no',
	'eif_feed_load_more_button_back_color'=> '#000000',
	'eif_feed_load_more_button_text_color' => '#fff',
	'eif_feed_load_more_button_text' => 'Load More',
	'eif_feed_header_status' =>'yes',
	'eif_feed_header_text_color' =>'#e34f0e',
	'eif_disable_light_box' =>'no',
	'eif_caption_status'=>'yes',
	'eif_caption_text_length' =>'30',
	'eif_caption_text_color' =>'#666',
	'eif_caption_text_size' =>'14',
	'eif_show_likes' =>'yes',
	'eif_show_comments' => 'yes',
	'eif_likes_comments_color' =>'#dd9494'
	
    );


// add the default settings value to the databsae
update_option('eif_settings',$eif_settings);
}


if(isset($_POST['submit2'])){

if(isset($_POST['eif_feed_load_more_button_status']))
{
$val = "yes";
}
else
{
$val = "no";
//print_r($val);
}
if(isset($_POST['eif_feed_show_button_status']))
{
$load_val = "yes";
}
else
{
$load_val = "no";

}

if(isset($_POST['eif_feed_header_status']))
{
$header_status = "yes";
}
else
{
$header_status = "no";
}

if(isset($_POST['eif_disable_light_box']))
{
$light_box_status = "yes";
}else{
$light_box_status = "no";
}

if(isset($_POST['eif_caption_status']))
{
$caption_status = "yes";
}else{
$caption_status = "no";
}
if(isset($_POST['eif_show_likes']))
{
$Likes_status = 'yes';
}else
{
$Likes_status = 'no';
}

if(isset($_POST['eif_show_comments']))
{
$comments_status ='yes';
}else
{
$comments_status = 'no';
}



    $eif_settings = array();
    $eif_settings = get_option('eif_settings');
    // update feeds section configuration settings.
    
    $eif_settings['eif_feed_width'] = $_POST['eif_feed_width'];
    $eif_settings['eif_feed_width_unit'] = $_POST['eif_feed_width_unit'];
    $eif_settings['eif_feed_height'] = $_POST['eif_feed_height'];
    $eif_settings['eif_feed_height_unit'] = $_POST['eif_feed_height_unit'];
    $eif_settings['eif_feed_background_color'] = $_POST['eif_feed_background_color'];
	$eif_settings['eif_feed_image_sorting'] = $_POST['eif_feed_image_sorting'];
	$eif_settings['eif_feed_number_of_images'] = $_POST['eif_feed_number_of_images'];
	$eif_settings['eif_feed_column_numbers'] = $_POST['eif_feed_column_numbers'];
	$eif_settings['eif_feed_image_resolution'] =$_POST['eif_feed_image_resolution'];
	$eif_settings['eif_feed_image_padding_unit'] = $_POST['eif_feed_image_padding_unit'];
	$eif_settings['eif_feed_image_padding_size']= $_POST['eif_feed_image_padding_size'];
    $eif_settings['eif_feed_show_button_status'] = $load_val;
	$eif_settings['eif_feed_button_background_color'] = $_POST['eif_feed_button_background_color'];
	$eif_settings['eif_feed_button_text_color'] = $_POST['eif_feed_button_text_color'];
    $eif_settings['eif_feed_follow_button_text'] = $_POST['eif_feed_follow_button_text'];
	$eif_settings['eif_feed_load_more_button_status'] = $val;
	$eif_settings['eif_feed_load_more_button_back_color'] = $_POST['eif_feed_load_more_button_back_color'];
	$eif_settings['eif_feed_load_more_button_text_color'] = $_POST['eif_feed_load_more_button_text_color'];
	$eif_settings['eif_feed_load_more_button_text'] = $_POST['eif_feed_load_more_button_text'];
	$eif_settings['eif_feed_header_status'] = $header_status;
	$eif_settings['eif_feed_header_text_color'] = $_POST['eif_feed_header_text_color'];
	$eif_settings['eif_disable_light_box'] = $light_box_status;
	$eif_settings['eif_caption_status'] = $caption_status;
	$eif_settings['eif_caption_text_length'] = $_POST['eif_caption_text_length'];
	$eif_settings['eif_caption_text_color'] = $_POST['eif_caption_text_color'];
	$eif_settings['eif_caption_text_size'] = $_POST['eif_caption_text_size'];
	$eif_settings['eif_show_likes'] = $Likes_status;
	$eif_settings['eif_show_comments'] = $comments_status;
	$eif_settings['eif_likes_comments_color'] = $_POST['eif_likes_comments_color'];
	
	
	//wp_die(print_r($eif_settings['eif_feed_background_color']));
       
        // update options
        update_option('eif_settings',$eif_settings);
        

}?>
<form id="eif_settings_form"  name="eif_form" method="post"><?php $eif_settings = get_option('eif_settings');  //wp_die(print_r(get_option('eif_settings'))); ?>
<h3><?php _e('Customize feed area','eif');?></h3>

<table class="form-table">
		
        <tr valign="top">
        <th scope="row"><label><?php _e('Width of feed area','eif');?></label></th>
            <td>
                <input type="text" id="eif_feed_width"  name="eif_feed_width" value="<?php esc_attr_e($eif_settings['eif_feed_width']); ?>" size="5" />
                <select name="eif_feed_width_unit">
                    <option value="px" <?php if($eif_settings['eif_feed_width_unit'] == "px") echo 'selected="selected"' ?> ><?php _e('px','eif'); ?></option>
                    <option value="%" <?php if($eif_settings['eif_feed_width_unit'] == "%") echo 'selected="selected"' ?> ><?php _e('%','eif'); ?></option>
                </select>
            </td>
        </tr>
         
        <tr valign="top">
        <th scope="row"><label><?php _e('Height of feed area','eif');?></label></th>
            <td>
                <input type="text" id="eif_feed_height"  name="eif_feed_height" value="<?php esc_attr_e($eif_settings['eif_feed_height']); ?>" size="5" />
                <select name="eif_feed_height_unit">
                    <option value="px" <?php if($eif_settings['eif_feed_height_unit'] == "px") echo 'selected="selected"' ?> ><?php _e('px','eif'); ?></option>
                    <option value="%" <?php if($eif_settings['eif_feed_height_unit'] == "%") echo 'selected="selected"' ?> ><?php _e('%','eif'); ?></option>
                </select>
            </td>
        </tr>
        
        <tr valign="top">
        <th scope="row"><label><?php _e('Background color of feed section','eif');?></label></th>
            <td>
                <input type="text" name="eif_feed_background_color" value="<?php esc_attr_e($eif_settings['eif_feed_background_color']);?>" class="eif-color-field" />
            </td>
        </tr>
		
       
    </table>
	<table class="form-table">
    <hr />
        <h3><?php _e('Photos','eif');?></h3>
		<tr valign="top">
			<th scope="row"><label><?php _e('Order by','eif');?></label></th>
				<td>
					<select name="eif_feed_image_sorting">
						<option value="newer" <?php if($eif_settings['eif_feed_image_sorting'] == "newer") echo 'selected="selected"' ?> ><?php _e('Newest to Oldest','eif'); ?></option>
						<option value="random" <?php if($eif_settings['eif_feed_image_sorting'] == "random") echo 'selected="selected"' ?> ><?php _e('Random','eif'); ?></option>
					</select>
				</td>	
		</tr>
		<tr>
			<th scope="row"><label><?php _e('Number of photos','eif');?></label></th>
			<td>
				<input type="text" id="eif_feed_number_of_images" name="eif_feed_number_of_images" value="<?php esc_attr_e($eif_settings['eif_feed_number_of_images']);?>">
				<br><br><span style="font-style:Italic;font-size:12px"><?php _e('Specify number of photos to show initially. Maximum 33 media items returned in a single request. In case of multiple id and hashtag, you will get, this much number of media items for each ids/tags.','eif');?></span>
			
			</td>
		</tr>
		<tr>
			<th scope="row"><label><?php _e('Number of columns','eif');?></label></th>
			<td>
				<select name="eif_feed_column_numbers">
					<option value="1"<?php  if($eif_settings['eif_feed_column_numbers']=="1") echo 'selected="selected"' ; ?>><?php _e('1','eif'); ?></option>
					<option value="2"<?php  if($eif_settings['eif_feed_column_numbers']=="2") echo 'selected="selected"' ; ?>><?php _e('2','eif'); ?></option>
					<option value="3"<?php  if($eif_settings['eif_feed_column_numbers']=="3") echo 'selected="selected"' ; ?>><?php _e('3','eif'); ?></option>
					<option value="4"<?php  if($eif_settings['eif_feed_column_numbers']=="4") echo 'selected="selected"' ; ?>><?php _e('4','eif'); ?></option>
					<option value="5"<?php  if($eif_settings['eif_feed_column_numbers']=="5") echo 'selected="selected"' ; ?>><?php _e('5','eif'); ?></option>
					<option value="6"<?php  if($eif_settings['eif_feed_column_numbers']=="6") echo 'selected="selected"' ; ?>><?php _e('6','eif'); ?></option>
					<option value="7"<?php  if($eif_settings['eif_feed_column_numbers']=="7") echo 'selected="selected"' ; ?>><?php _e('7','eif'); ?></option>
					<option value="8"<?php  if($eif_settings['eif_feed_column_numbers']=="8") echo 'selected="selected"' ; ?>><?php _e('8','eif'); ?></option>
					<option value="9"<?php  if($eif_settings['eif_feed_column_numbers']=="9") echo 'selected="selected"' ; ?>><?php _e('9','eif'); ?></option>
					<option value="10"<?php if($eif_settings['eif_feed_column_numbers']=="10") echo 'selected="selected"' ; ?>><?php _e('10','eif'); ?></option>
				</select>
			</td>
		</tr>
		
		<tr>
		<th scope="row"><label><?php _e('Image resolution','eif');?></label></th>
		<td>
			<select name="eif_feed_image_resolution">
				<option value="thumbnail"<?php if($eif_settings['eif_feed_image_resolution']=="thumbnail'") echo 'selected="selected"'; ?>><?php _e('Thumbnail (150*150)','eif');?></option>
				<option value="low_resolution"<?php if($eif_settings['eif_feed_image_resolution']=="low_resolution") echo 'selected="selected"'; ?>><?php _e('Medium (306*306)','eif');?></option>
				<option value="standard_resolution"<?php if($eif_settings['eif_feed_image_resolution']=='standard_resolution') echo 'selected="selected"'; ?>><?php _e('Full Size (640*640)','eif');?></option>
			</select>
		</td>
		</tr>
		
		<tr>
		<th scope="row"><label><?php _e('Padding around images','eif');?><label></th>
		<td>
			<input type="text" id="eif_feed_image_padding_size" name="eif_feed_image_padding_size" value="<?php esc_attr_e($eif_settings['eif_feed_image_padding_size']); ?>">
			<select name="eif_feed_image_padding_unit">
				<option value="px"<?php if($eif_settings['eif_feed_image_padding_unit']=='px') echo 'selected="selected"';?>><?php _e('px','eif');?></option>
				<option value="%"<?php if($eif_settings['eif_feed_image_padding_unit']=='%') echo 'selected="selected"';?>><?php _e('%','eif');?></option>
			</select>
		</td>
		</tr>
		<tr>
			<th scope="row"><label><?php _e('Enable Pop-up Lightbox','eif');?></label></th>
			<td>
				<input type="checkbox" name="eif_disable_light_box" value="yes" <?php if($eif_settings['eif_disable_light_box']=='yes'){echo 'checked';} ?>>
				<br><br><span style="font-style:Italic;font-size:12px"><?php _e('If you disable lightbox pop up than on click of each media item you will be redirected to Instagram page.','eif');?></span>
			</td>
		</tr>
		
 </table>
	
 <table class="form-table">
 <hr/>
	<h3><?php _e('Header','eif');?></h3>
	<tr valign="top">
		<th scope="row"><label><?php _e('Show the header','eif');?></label></th>
		<td>
			<input type="checkbox" name="eif_feed_header_status" value="yes" <?php if($eif_settings['eif_feed_header_status']=='yes'){echo 'checked';}?>>
		</td>
	<tr>
	<tr valign="top">
		 <th scope="row"><label><?php _e('Header text color','eif');?></label></th>
		 <td>
			<input type="text" name="eif_feed_header_text_color" value="<?php if(esc_attr_e($eif_settings['eif_feed_header_text_color']));?>" class="eif-color-field"/>
		 </td>
	</tr>
 </table>
 
 <table class="form-table">
 <hr/>
 <h3><?php _e('Caption','eif');?></h3>
 <tr valign="top">
	<th scope="row"><label><?php _e('Show caption','eif');?></label></th>
	<td>
			<input type="checkbox" name="eif_caption_status" value="yes" <?php if($eif_settings['eif_caption_status']=='yes'){echo 'checked';}?>>
	</td>
 </tr>
<tr>
	<th scope="row"><label><?php _e('Maximum text length','eif');?></label></th>
<td>
		<input type="text" name="eif_caption_text_length" value="<?php if(esc_attr_e($eif_settings['eif_caption_text_length']));?>" size="60">	
</td>
</tr>
<tr>
	<th scope="row"><label><?php _e('Text color','eif');?></label></th>
	<td>
		<input type="text" name="eif_caption_text_color" value="<?php if(esc_attr_e($eif_settings['eif_caption_text_color']));?>" class="eif-color-field"/>	
	</td>
</tr>
<tr>
	<th scope="row"><label><?php _e('Text size','eif');?></label></th>
			<td>
				<select name="eif_caption_text_size">
					<option value="10"<?php  if($eif_settings['eif_caption_text_size']=="10") echo 'selected="selected"' ; ?>><?php _e('10','eif'); ?></option>
					<option value="12"<?php  if($eif_settings['eif_caption_text_size']=="12") echo 'selected="selected"' ; ?>><?php _e('12','eif'); ?></option>
					<option value="14"<?php  if($eif_settings['eif_caption_text_size']=="14") echo 'selected="selected"' ; ?>><?php _e('14','eif'); ?></option>
					<option value="16"<?php  if($eif_settings['eif_caption_text_size']=="16") echo 'selected="selected"' ; ?>><?php _e('16','eif'); ?></option>
					<option value="18"<?php  if($eif_settings['eif_caption_text_size']=="18") echo 'selected="selected"' ; ?>><?php _e('18','eif'); ?></option>
					<option value="20"<?php  if($eif_settings['eif_caption_text_size']=="20") echo 'selected="selected"' ; ?>><?php _e('20','eif'); ?></option>
					<option value="22"<?php  if($eif_settings['eif_caption_text_size']=="22") echo 'selected="selected"' ; ?>><?php _e('22','eif'); ?></option>
					<option value="24"<?php  if($eif_settings['eif_caption_text_size']=="24") echo 'selected="selected"' ; ?>><?php _e('24','eif'); ?></option>
					<option value="26"<?php  if($eif_settings['eif_caption_text_size']=="26") echo 'selected="selected"' ; ?>><?php _e('26','eif'); ?></option>
					<option value="28"<?php  if($eif_settings['eif_caption_text_size']=="28") echo 'selected="selected"' ; ?>><?php _e('28','eif'); ?></option>
				</select>
			</td>
 </tr>
 </table>
 
 <table class="form-table">
		<hr/>
		<h3><?php _e('Likes and Comments','eif');?></h3>
		
	<tr valign="top">
		<th scope="row"><label><?php _e('Show number of likes','eif');?></label></th> 
		<td>
		<input type="checkbox" name="eif_show_likes" value="yes" <?php if($eif_settings['eif_show_likes']=='yes')
		{echo 'checked';}?>>
		
		</td>
	</tr>
	
	<tr valign="top">
		<th scope="row"><label><?php _e('Show number of comments','eif');?></label></th> 
		<td>
		<input type="checkbox" name="eif_show_comments" value="yes" <?php if($eif_settings['eif_show_comments']=='yes')
		{echo 'checked';}?>>
		</td>
	</tr>	
	
	
	<tr valign="top">
		<th scope="row"><label><?php _e('Background color','eif');?></label></th>
		<td>
		<input type="text" name="eif_likes_comments_color" value="<?php if(esc_attr_e($eif_settings['eif_likes_comments_color']));?>"  class="eif-color-field"/>
		</td>
	</tr>
 </table>
 
 <table class="form-table">
	 <hr/>
	 <h3><?php _e("'Load More' button",'eif');?> </h3>
	 <tr valign="top">
		<th scope="row"><label><?php _e("Show the 'Load More' button",'eif');?></label></th>
		<td>
			<input type="checkbox" name="eif_feed_load_more_button_status" value="yes" <?php if($eif_settings['eif_feed_load_more_button_status']=='yes'){echo 'checked';}?>>
		</td>
	 </tr>
	<tr valign="top">
		 <th scope="row"><label><?php _e('Button background color','eif');?></label></th>
		 <td>
			<input type="text" name="eif_feed_load_more_button_back_color" value="<?php if(esc_attr_e($eif_settings['eif_feed_load_more_button_back_color']));?>" class="eif-color-field"/>
		 </td>
	</tr>
	<tr valign="top">
		 <th scope="row"><label><?php _e('Button text color','eif');?></label></th>
		 <td>
			<input type="text" name="eif_feed_load_more_button_text_color" value="<?php if(esc_attr_e($eif_settings['eif_feed_load_more_button_text_color']));?>" class="eif-color-field"/>
		 </td>
	</tr>
	<tr valign ="top">
		<th scope="row"><label><?php _e('Button text','eif');?></label></th>
		<td>
			<input type="text" name="eif_feed_load_more_button_text" value="<?php if(esc_attr_e($eif_settings['eif_feed_load_more_button_text']));?>" size="60">
		</td>
	</tr>
 </table>
 
 <table class="form-table">
	<hr/>
	<h3><?php _e("'Follow on Instagram' button",'eif');?></h3>
	<tr valign="top">
		<th scope="row"><label><?php _e('Show the follow button','eif');?></label></th>
		<td>
			<input type="checkbox" name="eif_feed_show_button_status" value="yes"<?php if($eif_settings['eif_feed_show_button_status']=='yes'){echo 'checked';} ?>>
		
		</td>
	</tr>
	<tr valign="top">
		<th scope="row"><label><?php _e('Button background color','eif');?></label></th>
		<td>
			<input type="text" name="eif_feed_button_background_color" value="<?php if(esc_attr_e($eif_settings['eif_feed_button_background_color'])); ?>" class="eif-color-field">
		</td>
	</tr>
	<tr valign="top">
		<th scope="row"><label><?php _e('Button text color','eif');?></label></th>
		<td>
			<input type="text" name="eif_feed_button_text_color" value="<?php if(esc_attr_e($eif_settings['eif_feed_button_text_color']));?>" class="eif-color-field"/>
		</td>
	</tr>
	<tr valign ="top">
		<th scope="row"><label><?php _e('Button text','eif');?></label></th>
		<td>
			<input type="text" name="eif_feed_follow_button_text" value="<?php if(esc_attr_e($eif_settings['eif_feed_follow_button_text']));?>" size="60">
		</td>
	</tr>
	
 </table>
 

    <input type="submit" name="submit2" value="<?php _e('Save','eif'); ?>" class="button button-primary"/>
	<input type="submit" name="rest" value="<?php _e('Reset','eif'); ?>" class="button button-primary"/>
</form>