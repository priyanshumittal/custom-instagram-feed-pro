<?php 
   $eif_token_url = "https://instagram.com/oauth/authorize/?client_id=";
   $eif_token_url .= '44a5744739304a48af362318108030bc';
   $eif_token_url .= "&scope=basic+public_content&redirect_uri=http://webriti.com/easy-instagram-feed/eiflite/";
   $eif_token_url .= "?return_uri=".admin_url('admin.php?page=easy-instagram-feed');
   $eif_token_url .= "&response_type=token&state=".admin_url('admin.php?page=easy-instagram-feed')."";

if(isset($_POST['submit1'])){
	$eif_hastag = "";
	$eif_userids = "";
	$eif_useridhashtag = "";
	
    $eif_settings = get_option('eif_settings');
   //print_r($eif_settings);
    // create array of multiple ids
    //$eif_userids = explode(",",$_POST['eif_user_id']);
    
    // $eif_settings = array(
    //                 'eif_access_token' => $_POST['eif_access_token'],
    //                 'eif_user_id' => $eif_userids,
    //     );
	
	if(isset($_POST['eif_filter_type_userid_hashtag'])){$FilterSerchUserIdEnable = "yes";} else {$FilterSerchUserIdEnable = "no";}
	if(isset($_POST['eif_filter_type_comman_hashtag'])){$FilterCommanHashtagEnable = "yes";} else {$FilterCommanHashtagEnable = "no";}
	

	$selected_radio = $_POST['searchtype'];
    //print_r($selected_radio);
	if($selected_radio == "hastag")
	{
		$eif_hastag = $_POST['eif_hastag'];
		$eif_hastag = explode(",",$eif_hastag);
	}
	
	
    $eif_settings['eif_access_token'] = $_POST['eif_access_token'];
    
	$eif_settings['eif_hastag'] = $eif_hastag;
	$eif_settings['eif_serach_type'] = $selected_radio;
    $eif_settings['eif_filter_userid_hashtag_type'] = $FilterSerchUserIdEnable;
	$eif_settings['eif_filter_comman_hashtag_type'] = $FilterCommanHashtagEnable;
	$eif_settings['eif_Filter_Serch_UserId_Hashtag'] = $eif_useridhashtag;
	$eif_settings['eif_filter_feed_type'] = $_POST['eif_filter_feed_type'];
	
    // update options
    update_option('eif_settings',$eif_settings);
}?>
<form  name="eif_form" method="post"><?php $eif_settings = get_option('eif_settings'); //wp_die(print_r($eif_settings)); ?>
<h3><?php _e('Authorize plugin to get access token.','eif');?></h3>
<div class="eif_auth_button">
    <a href="<?php echo $eif_token_url;?>" class="button button-primary"><?php _e('Login to your account and authorize the app.','eif');?></a>
</div>
<table class="form-table">
        <tr valign="top">
        <th scope="row"><label><?php _e('Your access token','eif');?> </label></th>
		<?php if($eif_settings['eif_serach_type'] == 'user') { ?>
        <td><input type="text" id="eif_access_token"  name="eif_access_token" value="<?php esc_attr_e($eif_settings['eif_access_token']); ?>" class="eif_token" /><span style="font-style:Italic;font-style:italic;font-size:12px;"> </br><?php echo sprintf(__("To show multiple user feeds add multiple tokens using commas. Click the link to know token <a href='http://instagram.pixelunion.net/' target='_blank'>link</a> Like this:","eif","eif"));?></span> <br> <code>[easyinstagramfeed accesstoken="Token1"]</code><br />
        <code>[easyinstagramfeed accesstoken="Token,Token2"]</code></td>
		<?php } else {?>
		<td><input type="text" id="eif_access_token"  name="eif_access_token" value="<?php esc_attr_e($eif_settings['eif_access_token']); ?>" class="eif_token"/><span style="font-style:Italic;font-style:italic;font-size:12px;"></br> <?php echo sprintf(__("To show multiple user feeds add multiple tokens using commas. Click the link to know token <a href='http://instagram.pixelunion.net/' target='_blank'>link</a> Like this:","eif","eif"));?></span> <br> <code>[easyinstagramfeed accesstoken="Token1"]</code><br />
        <code>[easyinstagramfeed accesstoken="Token,Token2"]</code></td>
	   <?php }?>
	   </tr>
	   
        <tr valign="top">
        <th scope="row"><label><?php _e('User ID','eif');?></label></th>
        <td><input class="radio" type="radio" name="searchtype" checked value="user" <?php if($eif_settings['eif_serach_type']=='user'){echo 'checked';}?>><?php _e('Enable this radio button to show your user id feeds.','eif'); ?>
        
		<br>
		<span id="userhashtagsection"><input type="checkbox" name="eif_filter_type_userid_hashtag" value="yes" <?php if($eif_settings['eif_filter_userid_hashtag_type']=='yes'){echo 'checked';}?>>
		<?php _e('Filter user feed with the specific hashtags :','eif');?> <input type="text" id="eif_FilterSerchUserIdHashtag" name="eif_FilterSerchUserIdHashtag" value="<?php if($eif_settings['eif_filter_userid_hashtag_type'] == 'yes') { if($eif_settings['eif_Filter_Serch_UserId_Hashtag'] != '') {esc_attr_e(implode(',',$eif_settings['eif_Filter_Serch_UserId_Hashtag'])); }else {}}?>" /><span style="font-style:Italic;font-style:italic;font-size:12px;"> <?php _e('Seperate multiple Hashtag using commas','eif');?> </span>
		</span>
		</td>
	   </tr>
		<tr valign="top">
        <th scope="row"><label><?php _e('Hashtag','eif');?></label></th>
			<td><input class="radio" type="radio" name="searchtype" value="hastag" <?php if($eif_settings['eif_serach_type']=='hastag'){echo 'checked';}?>><input type="text" id="eif_hastag" name="eif_hastag" value="<?php if($eif_settings['eif_serach_type'] == "hastag") { esc_attr_e(implode(',',$eif_settings['eif_hastag'])); }else {}?>" />
				<span style="font-style:Italic;font-style:italic;font-size:12px;"> <?php _e("Separate multiple Hashtag using commas.<br><strong>Note:</strong> No need to add '#' symbol before any hashtag.","eif");?></span>
				<br>
			</td>
        </tr>
		<tr valign="top">
		<th scope="row"><label><?php _e('Display feeds common in multiple hashtags','eif');?></label></th>
		<td><input  type="checkbox" name="eif_filter_type_comman_hashtag" value="yes" <?php if($eif_settings['eif_filter_comman_hashtag_type'] == 'yes'){echo 'checked';}?>> &nbsp; <?php _e('Show common feeds tagged by multiple hashtags.','eif');?>
		</td>
		</tr>
		
		<tr valign="top">
		<th scope="row"><label><?php _e('Select type of media want to show in feeds','eif');?></label></th>
		<td>
			<select id="eif_filter_feed_type" name="eif_filter_feed_type">
				<option value="all" <?php if($eif_settings['eif_filter_feed_type']=='all'){ echo 'selected'; } ?>><?php _e('All','eif');?></option>
				<option value="image" <?php if($eif_settings['eif_filter_feed_type']=='image'){ echo 'selected'; } ?>><?php _e('Image','eif');?></option>
				<option value="video" <?php if($eif_settings['eif_filter_feed_type']=='video'){ echo 'selected'; } ?>><?php _e('Video','eif');?></option>
			</select>
			</td>
		</tr>
</table>
    <input type="submit" name="submit1" value="<?php _e('Save','eif'); ?>" class="button button-primary"/>
</form>

