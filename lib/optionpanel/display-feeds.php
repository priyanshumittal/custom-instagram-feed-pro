<h3><?php _e('Display your Feed','eif') ?></h3>
<p>
<?php _e("To show Instagram feeds on the pages or posts add the shortcode highlighted below in the editor. But for this configure the details in the option panel.","eif");?>
</p>
<input type="text" value="[easyinstagramfeed]" size="17" readonly="readonly" style="text-align: center;" onclick="this.focus();this.select()">
 <p><?php _e("The plugin comes with the number of attributes so that you can configure your feeds by just assigning the appropriate value to it. For example, if you'd like to display Instagram feed in the fashion of 4 columns than all you need to do is just assign the value 4 to the cols attribute as shown below.","eif"); ?>
<code>[easyinstagramfeed cols=4]</code></p>
        <p><?php _e("You can display as many different feeds as you like, on either the same page or on different pages, by just using the shortcode attributes below. For example:","eif");?><br />
        <code>[easyinstagramfeed]</code><br />
        <code>[easyinstagramfeed accesstoken="1591885187.44a5744.385971946cc341fa9f84967c9ba1b9db"]</code><br />
        <code>[easyinstagramfeed accesstoken="1591885187.44a5744.385971946cc341fa9f84967c9ba1b9db,1631861081.3a81a9f.e8ce23b5d41640cfbc39a320d5fdf6eb"]</code>
        
        </p>
<p><?php _e("See the table below for a full list of available shortcode attributes:","eif"); ?></p>
<table border="1" class="att_table">
<tbody>
	<tr valign="top">
		<th scope="row"><?php _e('Shortcode option','eif');?></th>
		<th scope="row"><?php _e('Description','eif');?></th>
		<th scope="row"><?php _e('Example','eif');?></th>
	</tr>
	<tr class="att_table_header">
		<td colspan=3>
		<?php _e('General Settings','eif');?>
		</td>
	</tr>
	<tr>
		<td><?php echo 'searchtype';?></td>
		<td><?php echo sprintf(__("This is to specify which type of feed you want to display. If you want to show feed with hashtag then specify <b>hashtag</b> as a value of shortcode attribute <i>searchtype</i>. If you want feeds from specific user account than specify <b>user</b> as a value of shortcode attribute <i>searchtype</i>. <br> Note: In case of user feed  it is not mandatory to use specify searchtype value as user, it is the defautl value ie [easyinstagramfeed userid='your-user-id' searchtype='user'] is equivalent to [easyinstagramfeed userid='your-user-id']","eif")); ?>
		</td>
		<td><code>[easyinstagramfeed searchtype="hashtag"]</code></td>
	</tr>
	
	<tr>
		<td>hashtag</td>
		<td><?php _e('An Instagram Hashtag. Want to fetch feeds from multiple Hashtag than specify their Name and for multiple hashtag separate them by commas. Also no need to put # symbol before the hashtag name.','eif'); ?></td>
		<td><code>[easyinstagramfeed searchtype="hashtag" hashtag="adidas"]</code></td>
	</tr>
	<tr>
		<td>mediatype</td>
		<td><?php _e('Show specific media type like image or video. if you want to show both media type image and video then set it all. mediatype="image/video/all"','eif'); ?></td>
		<td><code>[easyinstagramfeed mediatype="image"]</code></td>
	</tr>
	<tr>
		<td>accesstoken</td>
		<td><?php _e('An Instagram User Token. Want to fetch feeds from multiple user accounts than specify their Tokens and separate them by commas. Use this tool to know your <a href="http://instagram.pixelunion.net/" target="_blank">Token</a> . You can also get your token by authorizing this app in the General Settings of the Option Panel.','eif'); ?></td>
		<td><code>[easyinstagramfeed accesstoken="0000000000.000000000"]</code></td>
	</tr>
	<tr class="att_table_header">
		<td colspan=3>
		<?php _e('Customize feed area');?>
		</td>
	</tr>
	<tr>
		<td>width</td>
		<td><?php _e('The width of your feed in number. No need to add px or %. Default value is 100.','eif');?></td>
		<td><code>[easyinstagramfeed width=100]</code></td>
	</tr>
	<tr>
		<td>widthunit</td>
		<td><?php _e('The width unit of the feed. Allowed units are px or %. Default value if not specified is %.','eif');?></td>
		<td><code>[easyinstagramfeed widthunit="%"]</code></td>
	</tr>
	<tr>
		<td>height</td>
		<td><?php _e('The height of the feed. Just specify a number. The Default value is 100.','eif') ?></td>
		<td><code>[easyinstagramfeed height=100]</code></td>
	</tr>
	<tr>
		<td>heightunit</td>
		<td><?php _e('The height unit of feed. Use px or %.','eif');?></td>
		<td><code>[easyinstagramfeed heightunit="%"]</code></td>
	</tr>
	<tr>
		<td>backgroundcolor</td>
		<td><?php _e('The background color color of your feed area. Default value #fff.','eif');?></td>
		<td><code>[easyinstagramfeed backgroundcolor="#595e4d"]</code></td>
	</tr>
	
	<tr class="att_table_header">
	<td colspan=3><?php _e('Photos','eif')?></td>
	</tr>
	
	<tr>
	<td>orderby</td>
	<td><?php echo sprintf(__('Sort photos by order. It has two values <b>"Newest to Oldest"</b> and <b>"Random"</b>. Default order is <strong>Newest to Oldest</strong> if not specified.','eif'));?></td>
	<td><code>[easyinstagramfeed orderby="random"]</code></td>
	</tr>
	
	<tr>
	<td>num</td>
	<td><?php _e('Limit number of images to show.','eif');?></td>
	<td><code>[easyinstagramfeed num=5]</code></td>
	</tr>
	
	<tr>
	<td>cols</td>
	<td><?php _e('Number of column of feed. The default value is 4.','eif');?></td>
	<td><code>[easyinstagramfeed cols=4]</code></td>
	</tr>
	
	<tr>
	<td>resolution</td>
	<td><?php echo sprintf(__('Resolution of images, has three values <b>thumbnail</b> , <b>low_resolution</b> and <b>standard_resolution</b>. Default resolution is low_resolution if not specified. Thumbnail will render image of size 150 * 150, low resolution 306 * 306 and standard 640px * 640px ','eif'));?></td>
	<td><code>[easyinstagramfeed resolution="low_resolution"]</code></td>
	</tr>
	
	<tr>
	<td>imagepadding</td>
	<td><?php _e('The image padding size in number. The default is 5.','eif');?></td>
	<td><code>[easyinstagramfeed imagepadding=5]</code></td>
	</tr>
	
	<tr>
	<td>imagepaddingunit</td>
	<td><?php _e('The image padding unit in % or px. The default is px.','eif');?></td>
	<td><code>[easyinstagramfeed imagepaddingunit="px"]</code></td>
	</tr>
  
  <tr>
	<td>showlightbox</td>
	<td><?php echo sprintf(__("To disable popup set <strong>no</strong>. The default value is <b>yes</b>.","eif"));?></td>
	<td><code>[easyinstagramfeed showlightbox='yes']</code></td>
	</tr>
	
	
	<tr class="att_table_header">
	<td colspan=3><?php _e('Load More Button Settings','eif');?></td>
	</tr>
	
	<tr>
	<td>loadmorebuttondisplay</td>
	<td><?php echo sprintf(__("Whether to show the 'Load More' button. <b>yes</b> or <b>no</b>. The default is <b>no</b>.","eif"));?></td>
	<td><code>[easyinstagramfeed loadmorebuttondisplay="yes"]</code></td>
	</tr>
	
	<tr>
	<td>loadmorebuttoncolor</td>
	<td><?php _e('The Load More button color. The default is #000.','eif');?></td>
	<td><code>[easyinstagramfeed loadmorebuttoncolor="#000000"]</code></td>
	</tr>
	
	<tr>
	<td>loadmorebuttontextcolor</td>
	<td><?php _e('The Load More button text color. The default is #fff.','eif');?></td>
	<td><code>[easyinstagramfeed loadmorebuttontextcolor="#fff"]</code></td>
	</tr>
	
	<tr>
	<td>loadmorebuttontext</td>
	<td><?php _e('The Load More button display text.','eif');?></td>
	<td><code>[easyinstagramfeed loadmorebuttontext="Load More.."]</code></td>
	</tr>

	<tr class="att_table_header">
		<td colspan=3><?php _e('Follow on Instagram button configuration','eif'); ?></td>
	</tr>
	
	
	<tr>
	<td>followbuttondisplay</td>
	<td><?php echo sprintf(__("Whether to show the <b>Follow on Instagram</b> button. <b>yes</b> or <b>no</b>. The default value is no.","eif"));?></td>
	<td><code>[easyinstagramfeed followbuttondisplay="Yes"]</code></td>
	</tr>
	
	<tr>
	<td>followbuttoncolor</td>
	<td><?php _e('The follow on Instagram button color. The default color is #000.','eif');?></td>
	<td><code>[easyinstagramfeed followbuttoncolor="#000000"]</code></td>
	</tr>
	
	<tr>
	<td>followbuttontextcolor</td>
	<td><?php _e('The follow on Instagram button text color. The default is #fff.','eif');?></td>
	<td><code>[easyinstagramfeed followbuttontextcolor="#fff"]</code></td>
	</tr>
	
	<tr>
	<td>followbuttontext</td>
	<td><?php _e('The follow on Instagram button display text.','eif');?></td>
	<td><code>[easyinstagramfeed followbuttontext="Follo on Instagram.."]</code></td>
	</tr>
  
  
	<tr class="att_table_header">
		<td colspan=3><?php _e('Caption options'); ?></td>
	</tr>
  
  <tr>
	<td>captiondisplay</td>
	<td><?php echo sprintf(__("Whether to show the caption on feed. <b> yes</b> or <b>no</b>. The default value is no.","eif"));?></td>
	<td><code>[easyinstagramfeed captiondisplay="Yes"]</code></td>
	</tr>
  
  <tr>
	<td>captiontextlength</td>
	<td><?php _e('The caption text length. The default is 10.','eif');?></td>
	<td><code>[easyinstagramfeed captiontextlength="10"]</code></td>
	</tr>
  
  <tr>
	<td>captiontextcolor</td>
	<td><?php _e('The caption text color. The default is #666.','eif');?></td>
	<td><code>[easyinstagramfeed captiontextcolor="#666"]</code></td>
	</tr>
  
   <tr>
	<td>captiontextsize</td>
	<td><?php _e('The caption text size does not need to mention px it is by default include in. The default is 14.','eif');?></td>
	<td><code>[easyinstagramfeed captiontextsize="14"]</code></td>
	</tr>
  
  
  <tr class="att_table_header">
		<td colspan=3><?php _e('Header','eif'); ?></td>
	</tr>
  
  <tr>
	<td>headerdisplay</td>
	<td><?php echo sprintf(__("Whether to show the header on feed. <b>yes</b> or <b>no</b>. The default value is yes.","eif"));?></td>
	<td><code>[easyinstagramfeed headerdisplay="Yes"]</code></td>
	</tr>
  
  <tr>
	<td>headertextcolor</td>
	<td><?php _e('The color of header text. The default is #e34f0e.','eif');?></td>
	<td><code>[easyinstagramfeed headertextcolor="#e34f0e"]</code></td>
</tr>

<tr class="att_table_header">
		<td colspan=3><?php _e('Likes and comments','eif'); ?></td>
	</tr>
  
  <tr>
	<td>likesdisplay</td>
	<td><?php echo sprintf(__("Whether to show the likes on feed. <b>yes</b> or <b>no</b>.  The default value is yes.","eif"));?></td>
	<td><code>[easyinstagramfeed likesdisplay="Yes"]</code></td>
	</tr>
  
   <tr>
	<td>commentsdisplay</td>
	<td><?php echo sprintf(__("Whether to show the comments on feed. <b>yes</b> or <b>no</b>.  The default value is yes.","eif"));?></td>
	<td><code>[easyinstagramfeed commentsdisplay="Yes"]</code></td>
	</tr>
  
	<tr>
	<td>likescommentscolor</td>
	<td><?php _e('The likes and comments color. The default is #dd9494.','eif');?></td>
	<td><code>[easyinstagramfeed likescommentscolor="#dd9494"]</code></td>
	</tr>
	<tr class="att_table_header">
		<td colspan=3><?php _e('Filter feed','eif'); ?></td>
	</tr>
	<tr>
	<td>filteruserid</td>
	<td><?php _e('Enable user id filtering with hashtag.','eif');?></td>
	<td><code>[easyinstagramfeed filteruserid="yes"]</code></td>
	</tr>
	
	<tr>
	<td>taggedby</td>
	<td><?php _e('Enter the hashtag value that tagged user.','eif');?></td>
	<td><code>[easyinstagramfeed taggedby="adidas"]</code></td>
	</tr>
	
	<tr>
	<td>displaycomman</td>
	<td><?php _e('Enable disable common hashtag for both filtering.','eif');?></td>
	<td><code>[easyinstagramfeed displaycomman="yes"]</code></td>
	</tr>
	
	</tbody>
</table>
